#perl vs2015

how to build

So if you want to build a 32-bit perl:

1. cmd
2. "C:\Program Files (x86)\Microsoft Visual Studio 14.0\vc\vcvarsall.bat" x86
3. cd ./Projects/perl_vs/perl5-5.25.10/win32
4. uncomment the WIN64=undef in the Makefile file
5. change CCTYPE on  MSVC140
6. type nmake clean before first compilation and press enter 
7. execute nmake command
> nmake clean
> nmake 
you will see at the end of compilation something like this: Everything is up to date. 'nmake test' to run test suite.
8. execute nmake test command and see output like:
> nmake test
9. if you want to install perl, Please execute "nmake install" 
